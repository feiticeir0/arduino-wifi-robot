function stopRobot(){
	$.get( "update_state.php", { 
		left: "0", 
		right: "0", 
		forward: "0", 
		backward: "0"} );
}

function fullForward(){
	$.get( "update_state.php", { 
		left: "0", 
		right: "0", 
		forward: "1", 
		backward: "0"} );
}

function fullBackward(){
	$.get( "update_state.php", { 
		left: "0", 
		right: "0", 
		forward: "0", 
		backward: "1"} );
}


function turnLeft(){
	$.get( "update_state.php", { 
		left: "1", 
		right: "0", 
		forward: "0", 
		backward: "0"} );
}

function turnRight(){
	$.get( "update_state.php", { 
		left: "0", 
		right: "1", 
		forward: "0", 
		backward: "0"} );
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

