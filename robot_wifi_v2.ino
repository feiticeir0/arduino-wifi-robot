/* 
*  Simple robot control with Arduino & the CC3000 WiFi chip
*   Bruno Santos
* feiticeir0@whatgeek.com.pt
* GPL - You can use it freely 
* This is based on the excelent code by Marco Schwartz
* for Adafruit Learning Center
* https://learn.adafruit.com/wifi-controlled-mobile-robot/introduction
*/

// Include required libraries
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
#include "utility/debug.h"
#include <stdlib.h>

String result;
int motorCommand[4];


// Define CC3000 chip pins
#define ADAFRUIT_CC3000_IRQ   3
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10



// Motor pins
int enableA = 6;
int enableB = 9;

int pinA1 = 4;
int pinA2 = 2;
int pinB1 = 7;
int pinB2 = 8;

 
const unsigned long
  dhcpTimeout     = 60L * 1000L, // Max time to wait for address from DHCP
  connectTimeout  = 15L * 1000L, // Max time to wait for server connection
  responseTimeout = 15L * 1000L; // Max time to wait for data from server
uint32_t t;

int resultLength;

// WiFi network (change with your settings !)
#define WLAN_SSID       "Cybercentro1"        // cannot be longer than 32 characters!
#define WLAN_PASS       "#cybercentro#!"
#define WLAN_SECURITY   WLAN_SEC_WPA2 // This can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2

// What TCP port to listen on for connections.
#define LISTEN_PORT           8888    

// Create CC3000 instances
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIV2);                                
                                         
// Create server
Adafruit_CC3000_Server robotServer(LISTEN_PORT);

void setup() {
   
  Serial.begin(9600);
  
  result = "";
  
  pinMode (enableA, OUTPUT);
  pinMode (enableB, OUTPUT);
  pinMode(pinA1, OUTPUT);
  pinMode(pinA2, OUTPUT);
  pinMode(pinB1, OUTPUT);
  pinMode(pinB2, OUTPUT);
  
  /* Initialise the module */
  Serial.println(F("\nInitializing..."));
  if (!cc3000.begin())
  {
    Serial.println(F("Couldn't begin()! Check your wiring?"));
    while(1);
  }
  
  if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
    Serial.println(F("Failed!"));
    while(1);
  }
   
  Serial.println(F("Connected!"));
  
  Serial.println(F("Request DHCP"));
  while (!cc3000.checkDHCP())
  {
    delay(100); // ToDo: Insert a DHCP timeout!
  }  

  /* Display the IP address DNS, Gateway, etc. */  
  while (! displayConnectionDetails()) {
    delay(1000);
  }
 
  // Start listening for connections
  robotServer.begin();
  
  Serial.println(F("Listening for connections..."));
 
}

void loop() {
  
  //enable Motors
  enable();
  
  
  // Try to get a client which is connected.
  Adafruit_CC3000_ClientRef client = robotServer.available();
  if (client) {
     boolean currentLineIsBlank = true;
     // Check if there is data available to read.
     while (client.available()) {
     
       char c = client.read();
       result = result + c;
       Serial.write(c);
       
       // Delete HTTP headers
      if(result.endsWith("Content-Type: text/html"))
      {
        result="";
      }
       
       if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();          
       }
       if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
       
     }
     // give the web browser time to receive the data
    delay(5);
    // close the connection:
    client.close();
    Serial.println("client disconnected");
    
    Serial.print("result: ");
    Serial.println (result);
    // Format result and extract the variables
    format_result(motorCommand,result);
    
    Serial.print ("After format: ");
    Serial.println (String(motorCommand[0]) + ":" + String(motorCommand[1]) + ":" + String(motorCommand[2]) + ":" + String(motorCommand[3]));
 
    // Print received values
    //Serial.println("Motor 1 speed: " + String(motorCommand[0]) + " and direction: " + String(motorCommand[2]));
    //Serial.println("Motor 2 speed: " + String(motorCommand[1]) + " and direction: " + String(motorCommand[3]));

    // Send motor commands
    
    //send_motor_command(enableA,motorCommand[0],motorCommand[2]);
    //send_motor_command(enableB,motorCommand[1],motorCommand[3]);
    send_motor_command(motorCommand[0],motorCommand[1],motorCommand[2],motorCommand[3]);
    
    /*
    Serial.print ("Motor command 1: ");
    Serial.print (motorCommand[0]);
    Serial.println (motorCommand[2]);
    
    Serial.print ("Motor command 2: ");
    Serial.print (motorCommand[1]);
    Serial.println (motorCommand[3]);
   */
    // Reset result variable
    result = "";
  }
           
}

void send_motor_command(int left, int right, int fwd, int bwd) {
 
  //lets check what we have
  if (fwd) {
    forward();
    //delay(1000); //1s
  }
  
  if (bwd) {
    backward();
    //delay (500); // 1/2s
  }
  
  if (left) {
    turnLeft();
  }
  
  if (right) {
    turnRight();
  }
  
  //stop
  if ((!left) && (!right) && (!fwd) && (!bwd)) {
    stopRobot();
  }
}

void format_result(int* array, String result) {
 
 result.trim();
 resultLength = result.length();
 Serial.println(result);
 
 int commaPosition;
 int i = 0;
 do
  {
      commaPosition = result.indexOf(',');
      if(commaPosition != -1)
      {
          Serial.println( result.substring(0,commaPosition));
          array[i] = result.substring(0,commaPosition).toInt();
          i = i+1;
          result = result.substring(commaPosition+1, result.length());
      }
      else
      {
         if(result.length() > 0) {
           Serial.println(result);
          }
      }
      
   }
   while(commaPosition >=0);  
} 

/**************************************************************************/
/*!
    @brief  Tries to read the IP address and other connection details
*/
/**************************************************************************/
bool displayConnectionDetails(void)
{
  uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;
  
  if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
  {
    Serial.println(F("Unable to retrieve the IP Address!\r\n"));
    return false;
  }
  else
  {
    Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
    Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
    Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
    Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
    Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
    Serial.println();
    return true;
  }
}

void enable() {
  digitalWrite (enableA, HIGH);
  digitalWrite (enableB, HIGH);
}

void disable() {
  digitalWrite (enableA, LOW);
  digitalWrite (enableB, LOW);
}

//function direction
void backward() {
  digitalWrite (pinA1, LOW);
  digitalWrite (pinA2, HIGH);
  digitalWrite (pinB1, HIGH);  
  digitalWrite (pinB2, LOW);  
}

void forward() {
  digitalWrite (pinA1, HIGH);
  digitalWrite (pinA2, LOW);
  digitalWrite (pinB1, LOW);  
  digitalWrite (pinB2, HIGH);
}

void turnRight() {
  //motorA backward
  digitalWrite (pinA1, LOW);
  digitalWrite (pinA2, HIGH);
  //motorB forward
  digitalWrite (pinB1, LOW);  
  digitalWrite (pinB2, HIGH);
  //delay for 1/2s is enough
  delay(500);
  stopRobot();  
}

void turnLeft() {
  //motorA forward
  digitalWrite (pinA1, HIGH);
  digitalWrite (pinA2, LOW);
  //motorB backward
  digitalWrite (pinB1, HIGH);  
  digitalWrite (pinB2, LOW);
  //delay for 1/2s is enough
  delay(500);
  stopRobot();  

}

void stopRobot() {
  digitalWrite (pinA1, HIGH);
  digitalWrite (pinA2, HIGH);
  digitalWrite (pinB1, HIGH);  
  digitalWrite (pinB2, HIGH);    
}
