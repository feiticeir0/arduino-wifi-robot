<?php

	// Load JSON state
    $string = file_get_contents("robot_state.json");
    $json_a= json_decode($string,true);

    // Handle GET request
    $json_a['left'] = $_GET["left"];
    $json_a['right'] = $_GET["right"];
    $json_a['forward'] = $_GET["forward"];
    $json_a['backward'] = $_GET["backward"];

    // Save JSON file
    $fp = fopen('robot_state.json', 'w');
    fwrite($fp, json_encode($json_a));
    fclose($fp);

    // Create a TCP/IP socket & connect to the server
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    $result = socket_connect($socket, "172.20.1.109", "8888");

    // Request
    $in = "HEAD / HTTP/1.1\r\n";
    $in .= "Content-Type: text/html\r\n";
    $in .= $json_a['left'] . "," . 
    $json_a['right'] . "," .
    $json_a['forward'] . "," .
    $json_a['backward'] . ",\r\n\r\n";
    $out = '';

    // Send request
    socket_write($socket, $in, strlen($in));

    // Read answer
    while ($out = socket_read($socket, 4096)) {
        echo $out;
    }

    // Close socket
    socket_close($socket);

?>
